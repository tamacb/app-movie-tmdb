import 'package:flutter/material.dart';
import 'package:movieappbloc/model/cast_response.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/repos/repository.dart';
import 'package:rxdart/rxdart.dart';

class SimilaryMovieBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<MovieResponse> _subject =
  BehaviorSubject<MovieResponse>();

  getSimilaryMovie(int id) async {
    MovieResponse response = await _repository.getSimilaryMovie(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject => _subject;
}

final castBloc = SimilaryMovieBloc();
