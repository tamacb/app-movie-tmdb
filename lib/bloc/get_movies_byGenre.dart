import 'package:flutter/cupertino.dart';
import 'package:movieappbloc/bloc/get_genre_bloc.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/repos/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieListByGenreBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<MovieResponse> _subject =
      BehaviorSubject<MovieResponse>();

  getMoviesByGenre(int id) async {
    MovieResponse response = await _repository.getMoviesByGenre(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject => _subject;
}

final movieByGenreBehavior = MovieListByGenreBloc();
