import 'package:movieappbloc/bloc/get_genre_bloc.dart';
import 'package:movieappbloc/model/genre_respone.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/repos/repository.dart';
import 'package:rxdart/rxdart.dart';

class GenreListBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<GenreResponse> _subject = BehaviorSubject<GenreResponse>();

  getGenres() async {
    GenreResponse response = await _repository.getGenre();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<GenreResponse> get subject => _subject;
}
final genresBloc = GenreListBloc();