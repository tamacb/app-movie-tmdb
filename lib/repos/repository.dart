import 'package:dio/dio.dart';
import 'package:movieappbloc/model/cast_response.dart';
import 'package:movieappbloc/model/genre_respone.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/model/movies_details_response.dart';
import 'package:movieappbloc/model/person_response.dart';
import 'package:movieappbloc/model/video_response.dart';

class MovieRepository {
  final String apiKey = "131ba6b9d023b53099f7cf48af3176e0";
  static String mainUrl = "https://api.themoviedb.org/3";
  final Dio _dio = Dio();

  var getPopularityUrl = '$mainUrl/movie/top_rated?';
  var getMoviesUrl = '$mainUrl/discover/movie?';
  var getPlayingUrl = '$mainUrl/movie/now_playing?';
  var getGenreUrl = '$mainUrl/genre/list?';
  var getPersonUrl = '$mainUrl/trending/person/week?';
  var getMovieDetailsUrl = '$mainUrl/movie';

  Future<MovieResponse> getMovies() async {
    var params = {"api_key": apiKey, "language": "en-US", "page": 1};
    try {
      Response response =
          await _dio.get(getPopularityUrl, queryParameters: params);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return MovieResponse.withError(error);
    }
  }

  Future<MovieResponse> getPlayingMovies() async {
    var params = {"api_key": apiKey, "language": "en-US", "page": 1};
    try {
      Response response =
          await _dio.get(getPlayingUrl, queryParameters: params);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return MovieResponse.withError(error);
    }
  }

  Future<GenreResponse> getGenre() async {
    var params = {"api_key": apiKey, "language": "en-US", "page": 1};
    try {
      Response response = await _dio.get(getGenreUrl, queryParameters: params);
      return GenreResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return GenreResponse.withError(error);
    }
  }

  Future<PersonResponse> getPerson() async {
    var params = {"api_key": apiKey};
    try {
      Response response = await _dio.get(getPersonUrl, queryParameters: params);
      return PersonResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return PersonResponse.withError(error);
    }
  }

  Future<MovieResponse> getMoviesByGenre(int id) async {
    var params = {
      "api_key": apiKey,
      "language": "en-US",
      "page": 1,
      "with_genres": id
    };
    try {
      Response response =
          await _dio.get(getMoviesUrl, queryParameters: params);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return MovieResponse.withError(error);
    }
  }

  Future<MovieResponse> getTopPlayingMovies() async {
    var params = {"api_key": apiKey, "language": "en-US", "page": 1};
    try {
      Response response =
      await _dio.get(getPlayingUrl, queryParameters: params);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return MovieResponse.withError(error);
    }
  }

  Future<MovieDetailsResponse> getDetailsMovie(int id) async {
    var params = {"api_key": apiKey, "language": "en-US"};
    try {
      Response response =
      await _dio.get(getMovieDetailsUrl + "/$id", queryParameters: params);
      return MovieDetailsResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return MovieDetailsResponse.withError(error);
    }
  }

  Future<CastResponse> getCast(int id) async {
    var params = {"api_key": apiKey, "language": "en-US"};
    try {
      Response response =
      await _dio.get(getMovieDetailsUrl + "/$id" + "/credits", queryParameters: params);
      return CastResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return CastResponse.withError(error);
    }
  }

  Future<MovieResponse> getSimilaryMovie(int id) async {
    var params = {"api_key": apiKey, "language": "en-US"};
    try {
      Response response =
      await _dio.get(getMovieDetailsUrl + "/$id" + "/similar", queryParameters: params);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return MovieResponse.withError(error);
    }
  }

  Future<VideoResponse> getVideosMovie(int id) async {
    var params = {"api_key": apiKey, "language": "en-US"};
    try {
      Response response =
      await _dio.get(getMovieDetailsUrl + "/$id" + "/videos", queryParameters: params);
      return VideoResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("ini lagi eror yaaa : $error ini stacktracenya $stacktrace");
      return VideoResponse.withError(error);
    }
  }
}
