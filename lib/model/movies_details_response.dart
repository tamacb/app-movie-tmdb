import 'package:movieappbloc/model/movie_details.dart';

class MovieDetailsResponse {
  final MovieDetails movieDetails;
  final String error;

  MovieDetailsResponse(this.movieDetails, this.error);

  MovieDetailsResponse.fromJson(Map<String, dynamic> json)
      : movieDetails = MovieDetails.fromJson(json),
        error = "";

  MovieDetailsResponse.withError(String errorVal)
      : movieDetails = MovieDetails(null, null, null, null, "", null),
        error = errorVal;
}
