import 'dart:convert';

import 'package:movieappbloc/model/genres.dart';

class MovieDetails {
  final int id;
  final int budget;
  final bool adult;
  final List<Genre> genre;
  final String realeaseDate;
  final int runTime;

  MovieDetails(this.id, this.budget, this.adult, this.genre, this.realeaseDate,
      this.runTime);

  MovieDetails.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        budget = json["budget"],
        adult = json['adult'],
        genre = (json['genre'] as List)
            .map((e) => new Genre.fromJson(json))
            .toList(),
        realeaseDate = json['realease'],
        runTime = json["runtime"];
  
}
