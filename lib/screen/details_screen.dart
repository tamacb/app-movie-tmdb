import 'package:flutter/material.dart';
import 'package:movieappbloc/bloc/get_movie_videos.dart';
import 'package:movieappbloc/model/movie.dart';
import 'package:movieappbloc/model/video.dart';
import 'package:movieappbloc/model/video_response.dart';
import 'package:sliver_fab/sliver_fab.dart';

class MovieDetailsPage extends StatefulWidget {
  final Movie movie;

  MovieDetailsPage({Key key, @required this.movie}) : super(key: key);

  @override
  _MovieDetailsPageState createState() => _MovieDetailsPageState(movie);
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  final Movie movie;

  _MovieDetailsPageState(this.movie);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    movieVideoBloc..getMovieVideo(movie.id);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    movieVideoBloc..dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellowAccent,
      body: Builder(
        builder: (context) {
          return SliverFab(
            floatingWidget: StreamBuilder<VideoResponse>(
              stream: movieVideoBloc.subject.stream,
              builder: (context, AsyncSnapshot<VideoResponse> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.error != null &&
                      snapshot.data.error.length > 0) {
                    return _buildErrorWidget(snapshot.data.error);
                  }
                  return _buildVideoWidget(snapshot.data);
                } else if (snapshot.hasError) {
                  return _buildErrorWidget(snapshot.error);
                } else {
                  return _buildLoadingWidget();
                }
              },
            ),
            expandedHeight: 200.0,
            slivers: [
              SliverAppBar(
                backgroundColor: Colors.white12,
                expandedHeight: 200.0,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text(
                    movie.title.length > 40
                        ? movie.title.substring(0, 37) + "..."
                        : movie.title,
                    style:
                        TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0),
                  ),
                  background: Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://image.tmdb.org/t/p/original/" +
                                        movie.backPoster))),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white70.withOpacity(0.3)),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                                colors: [
                              Colors.black12.withOpacity(0.3),
                              Colors.white70
                            ])),
                      )
                    ],
                  ),
                ),
              ),
              SliverPadding(
                padding: EdgeInsets.all(0.0),
                sliver: SliverList(
                  delegate: SliverChildListDelegate([
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 20.0),
                      child: Column(
                        children: [
                          Text("ratings"),
                          Text(
                            movie.overview,
                          )
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildVideoWidget(VideoResponse data) {
    List<Video> videos = data.videos;
    return Container();
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        children: [Text(error)],
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Container();
  }
}
