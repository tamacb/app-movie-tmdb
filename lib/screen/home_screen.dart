import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'file:///D:/flutterApp/movie_app_bloc/lib/screen/widget/genre/genres_widget.dart';
import 'file:///D:/flutterApp/movie_app_bloc/lib/screen/widget/nowPlaying/now_playing_widget.dart';
import 'package:movieappbloc/screen/widget/person/person_list.dart';
import 'package:movieappbloc/screen/widget/topmovies/top_movies.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white12,
      appBar: AppBar(
        backgroundColor: Colors.black12,
        centerTitle: true,
        leading: Icon(EvaIcons.menu2Outline, color: Colors.white,),
        title: Text("Movie Alakadar"),
        actions: [
          IconButton(
            icon: Icon(EvaIcons.searchOutline, color: Colors.white,), onPressed: null,
          )
        ],
      ),
      body: ListView(
        children: [
          NowPlaying(),
          GenresWidget(),
          PersonList(),
          TopMovies()
        ],
      ),
    );
  }
}
