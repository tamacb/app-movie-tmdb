import 'package:flutter/material.dart';
import 'package:movieappbloc/bloc/get_movies_byGenre.dart';
import 'package:movieappbloc/model/movie.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:movieappbloc/screen/details_screen.dart';

class GenreMovies extends StatefulWidget {
  final int genreId;
  GenreMovies({Key key, @required this.genreId});
  @override
  _GenreMoviesState createState() => _GenreMoviesState(genreId);
}

class _GenreMoviesState extends State<GenreMovies> {
  final int genreId;
  _GenreMoviesState(this.genreId);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    movieByGenreBehavior..getMoviesByGenre(genreId);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
      stream: movieByGenreBehavior.subject.stream,
      builder: (context, AsyncSnapshot<MovieResponse> snapshotMovieByGenre) {
        if (snapshotMovieByGenre.hasData) {
          if (snapshotMovieByGenre.data.error != null &&
              snapshotMovieByGenre.data.error.length > 0) {
            return _buildErrorWidget(snapshotMovieByGenre.data.error);
          }
          return _buildMovieByGenreWidget(snapshotMovieByGenre.data);
        } else if (snapshotMovieByGenre.hasError) {
          return _buildErrorWidget(snapshotMovieByGenre.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        children: [Text(error)],
      ),
    );
  }

  Widget _buildMovieByGenreWidget(MovieResponse data) {
    List<Movie> movies = data.movies;
    if (movies.length == 0) {
      return Container(
        child: Text("No data"),
      );
    } else
      return Container(
        height: 400.0,
        padding: EdgeInsets.only(left: 10.0),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: movies.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0, right: 10.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              MovieDetailsPage(movie: movies[index])));
                },
                child: Column(
                  children: [
                   ( movies[index].poster == null)
                        ? Container(
                            width: 120.0,
                            height: 190.0,
                            decoration: BoxDecoration(
                                color: Colors.white12,
                                borderRadius: BorderRadius.circular(2.0),
                                shape: BoxShape.rectangle),
                          )
                        : Container(
                            width: 120.0,
                            height: 180.0,
                            decoration: BoxDecoration(
                                color: Colors.white12,
                                borderRadius: BorderRadius.circular(2.0),
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        "https://image.tmdb.org/t/p/w200" +
                                            movies[index].poster),
                                    fit: BoxFit.cover)),
                          ),
                    Container(
                      width: 100.0,
                      child: Text(
                        movies[index].title,
                        maxLines: 2,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 11.0,
                            color: Colors.white),
                      ),
                    ),
                    Container(
                      width: 100.0,
                      child: Text(
                        "Popularity : " + movies[index].popularity.toString(),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 11.0,
                            color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      );
  }
}
