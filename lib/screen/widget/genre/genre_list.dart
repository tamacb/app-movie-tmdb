import 'package:flutter/material.dart';
import 'package:movieappbloc/bloc/get_movies_byGenre.dart';
import 'package:movieappbloc/model/genres.dart';
import 'package:movieappbloc/screen/widget/genre/genre_movies_widget.dart';


class GenreList extends StatefulWidget {
  final List<Genre> genres;
  GenreList({Key key, @required this.genres}) : super(key: key);
  @override
  _GenreListState createState() => _GenreListState(genres);
}

class _GenreListState extends State<GenreList>
    with SingleTickerProviderStateMixin {
  final List<Genre> genres;
  TabController _tabController;

  _GenreListState(this.genres);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(vsync: this, length: genres.length);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging){
        movieByGenreBehavior..drainStream();
      };
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 307.0,
      child: DefaultTabController(
        length: genres.length,
        child: Scaffold(
          backgroundColor: Colors.white12,
          appBar: PreferredSize(
            child: AppBar(
              backgroundColor: Colors.white12,
              bottom: TabBar(
                controller: _tabController,
                indicatorColor: Colors.yellow,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorWeight: 3.0,
                unselectedLabelColor: Colors.white70,
                labelColor: Colors.pink,
                isScrollable: true,
                tabs: genres
                    .map((Genre genre) => Container(
                          padding: EdgeInsets.only(bottom: 15.0, top: 10.0),
                          child: Text(
                            genre.name.toUpperCase(),
                            style: TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.w600),
                          ),
                        ))
                    .toList(),
              ),
            ),
            preferredSize: Size.fromHeight(50.0),
          ),
          body: TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: genres
                .map((Genre genre) => GenreMovies(genreId: genre.id))
                .toList(),
          ),
        ),
      ),
    );
  }
}
