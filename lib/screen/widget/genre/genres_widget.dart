import 'package:flutter/material.dart';
import 'package:movieappbloc/bloc/get_genre_bloc.dart';
import 'package:movieappbloc/model/genre_respone.dart';
import 'package:movieappbloc/model/genres.dart';
import 'file:///D:/flutterApp/movie_app_bloc/lib/screen/widget/genre/genre_list.dart';

class GenresWidget extends StatefulWidget {
  @override
  _GenresWidgetState createState() => _GenresWidgetState();
}

class _GenresWidgetState extends State<GenresWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    genresBloc..getGenres();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<GenreResponse>(
      stream: genresBloc.subject.stream,
      builder: (context, AsyncSnapshot<GenreResponse> snapshotGenres) {
        if (snapshotGenres.hasData) {
          if (snapshotGenres.data.error != null && snapshotGenres.data.error.length > 0) {
            return _buildErrorWidget(snapshotGenres.data.error);
          }
          return _buildGenreWidget(snapshotGenres.data);
        } else if (snapshotGenres.hasError) {
          return _buildErrorWidget(snapshotGenres.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }
  Widget _buildErrorWidget(String data) {
    return Center(
      child: Column(
        children: [
          Text(data)
        ],
      ),
    );
  }
  Widget _buildGenreWidget(GenreResponse data) {
    List<Genre> genres = data.genres;
    if(genres.length == 0){
      return Container(
        child: Text("No Genres"),
      );
    } else return GenreList(genres: genres);
  }
}
