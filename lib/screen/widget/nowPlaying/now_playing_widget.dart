import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movieappbloc/bloc/get_now_playing.dart';
import 'package:movieappbloc/model/movie.dart';
import 'package:movieappbloc/model/movie_response.dart';
import 'package:page_indicator/page_indicator.dart';

class NowPlaying extends StatefulWidget {
  @override
  _NowPlayingState createState() => _NowPlayingState();
}

class _NowPlayingState extends State<NowPlaying> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nowPlayingBehaviorBloc..getPlayingMovies();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
      stream: nowPlayingBehaviorBloc.subject.stream,
      builder: (context, AsyncSnapshot<MovieResponse> snapshotNowPlaying) {
        if (snapshotNowPlaying.hasData) {
          if (snapshotNowPlaying.data.error != null && snapshotNowPlaying.data.error.length > 0) {
            return _buildErrorWidget(snapshotNowPlaying.data.error);
          }
          return _buildNowPlayingWidget(snapshotNowPlaying.data);
        } else if (snapshotNowPlaying.hasError) {
          return _buildErrorWidget(snapshotNowPlaying.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String data) {
    return Center(
      child: Column(
        children: [Text(data)],
      ),
    );
  }
  Widget _buildNowPlayingWidget(MovieResponse data) {
    List<Movie> movies = data.movies;
    if (movies.length == 0) {
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [Text("No Movies")],
        ),
      );
    } else
      return Container(
        height: 220.0,
        child: PageIndicatorContainer(
            align: IndicatorAlign.bottom,
            indicatorSpace: 8.0,
            padding: EdgeInsets.all(5.0),
            indicatorColor: Colors.yellowAccent,
            indicatorSelectorColor: Colors.white70,
            length: movies.take(5).length,
            pageView: PageView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: movies.take(5).length,
              itemBuilder: (context, index) => Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 220.0,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            image: NetworkImage(
                                "https://image.tmdb.org/t/p/original" +
                                    movies[index].backPoster))),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Colors.black12.withOpacity(1.0),
                              Colors.white12.withOpacity(0.0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            stops: [0.0, 0.9])),
                  ),
                  Positioned(
                    bottom: 30.0,
                    child: Container(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Column(
                        children: [
                          Text(movies[index].title, style: TextStyle(color: Colors.white, fontSize: 16.0),)
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    child: Icon(FontAwesomeIcons.playCircle, color: Colors.yellow,size: 40.0,),
                    bottom: 0,
                    left: 0,
                    top: 0,
                    right: 0,
                  )
                ],
              ),
            )),
      );
  }
}
