import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movieappbloc/bloc/get_person_bloc.dart';
import 'package:movieappbloc/model/person.dart';
import 'package:movieappbloc/model/person_response.dart';

class PersonList extends StatefulWidget {
  @override
  _PersonListState createState() => _PersonListState();
}

class _PersonListState extends State<PersonList> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    personBloc..getPerson();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 10.0, top: 20.0),
          child: Text(
            "TRENDING TALENT OF THE WEEK",
            style: TextStyle(color: Colors.white, fontSize: 16.0),
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        StreamBuilder<PersonResponse>(
          stream: personBloc.subject.stream,
          builder: (context, AsyncSnapshot<PersonResponse> snapshotPerson) {
            if (snapshotPerson.hasData) {
              if (snapshotPerson.data.error != null &&
                  snapshotPerson.data.error.length > 0) {
                return _buildErrorWidget(snapshotPerson.data.error);
              }
              return _buildPersonWidget(snapshotPerson.data);
            } else if (snapshotPerson.hasError) {
              return _buildErrorWidget(snapshotPerson.error);
            } else {
              return _buildLoadingWidget();
            }
          },
        )
      ],
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String data) {
    return Center(
      child: Column(
        children: [Text(data)],
      ),
    );
  }

  Widget _buildPersonWidget(PersonResponse data) {
    List<Person> person = data.persons;
    return Container(
      height: 130.0,
      padding: EdgeInsets.only(left: 10.0),
      child: ListView.builder(
        itemCount: person.take(20).length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Container(
            width: 100.0,
            padding: EdgeInsets.only(top: 10.0, right: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                (person[index].profileImg == null)
                    ? Container(
                        width: 70.0,
                        height: 70.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        child: Icon(
                          FontAwesomeIcons.userAlt,
                          color: Colors.grey,
                        ),
                      )
                    : Container(
                        width: 70.0,
                        height: 70.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://image.tmdb.org/t/p/w200" +
                                        person[index].profileImg),
                                fit: BoxFit.cover)),
                      ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  person[index].name,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 9.0,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Trending for" + person[index].known,
                  style: TextStyle(fontSize: 7.0, fontWeight: FontWeight.w500, color: Colors.white),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
